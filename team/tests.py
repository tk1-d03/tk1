from django.test import TestCase, Client, RequestFactory
from django.contrib.auth.models import User
from django.contrib.auth import login, logout, authenticate, get_user
from django.http import HttpRequest
from django.contrib.sessions.backends.db import SessionStore

# Models
from .models import Team

# Views
from .views import dashboard, create_team, add_team

class SmokeTest(TestCase):
  def test_smoke(self):
    self.assertEquals(1, 1)

class TeamUnitTestURL(TestCase):
  def setUp(self):
    User.objects.create_user('username', password='rahasia123', email='email@email.com')

  def test_main_dashboard_url_if_user_is_logged_in(self):
    self.client.login(username='username', password='rahasia123')
    response = self.client.get('/teams/')
    self.assertEquals(response.status_code, 200)

  def test_main_dashboard_url_if_user_is_logged_out(self):
    self.client.logout()
    response = self.client.get('/teams/')
    self.assertEquals(response.status_code, 302)

  def test_main_dashboard_url_template(self):
    self.client.login(username='username', password='rahasia123')
    response = self.client.get('/teams/')
    self.assertTemplateUsed(response, 'team/myteams.html')

  def test_create_teams_url_if_user_is_logged_in(self):
    self.client.login(username='username', password='rahasia123')
    response = self.client.get('/create-team/')
    self.assertEquals(response.status_code, 200)

  def test_create_teams_url_if_user_is_logged_out(self):
    self.client.logout()
    response = self.client.get('/create-team/')
    self.assertEquals(response.status_code, 302)

  def test_create_teams_url_template(self):
    self.client.login(username='username', password='rahasia123')
    response = self.client.get('/create-team/')
    self.assertTemplateUsed(response, 'team/create-team.html')

class TeamUnitTestView(TestCase):
  def setUp(self):
    self.user = User.objects.create_user('username', password='rahasia123', email='email@email.com')
    self.factory = RequestFactory()

  def test_main_dashboard_view_response_if_user_is_logged_in(self):
    request = HttpRequest()
    request.user = authenticate(request, username='username', password='rahasia123')
    request.session = SessionStore(session_key=None)
    login(request, request.user)
    self.assertEquals(dashboard(request).status_code, 200)

  def test_main_dashboard_view_response_if_user_is_logged_out(self):
    request = self.factory.get('/teams/')
    request.user = self.user
    request.session = SessionStore(session_key=None)
    logout(request)
    self.assertEquals(dashboard(request).status_code, 302)

  def test_create_teams_view_response_if_user_is_logged_in(self):
    request = HttpRequest()
    request.user = authenticate(request, username='username', password='rahasia123')
    request.session = SessionStore(session_key=None)
    login(request, request.user)
    self.assertEquals(create_team(request).status_code, 200)

  def test_create_teams_view_response_if_user_is_logged_out(self):
    request = self.factory.get('/teams/')
    request.user = authenticate(request, username='username1', password='rahasia123')
    request.session = SessionStore(session_key=None)
    logout(request)
    self.assertEquals(create_team(request).status_code, 302)

  def test_add_teams_view_response_if_user_is_logged_in(self):
    request = HttpRequest()
    request.user = authenticate(request, username='username', password='rahasia123')
    request.session = SessionStore(session_key=None)
    login(request, request.user)
    self.assertEquals(add_team(request).status_code, 405)

  def test_add_teams_view_response_if_user_is_logged_out(self):
    request = self.factory.get('/teams/')
    request.user = authenticate(request, username='username1', password='rahasia123')
    request.session = SessionStore(session_key=None)
    logout(request)
    self.assertEquals(add_team(request).status_code, 302)

  def test_add_teams_view_post_without_data_if_user_is_logged_in(self):
    request = HttpRequest()
    request.user = authenticate(request, username='username', password='rahasia123')
    request.session = SessionStore(session_key=None)
    login(request, request.user)
    request.method = 'POST'
    self.assertEquals(add_team(request).status_code, 400)

  def test_add_teams_view_post_with_data_if_user_is_logged_in(self):
    request = HttpRequest()
    request.user = authenticate(request, username='username', password='rahasia123')
    request.session = SessionStore(session_key=None)
    login(request, request.user)
    request.method = 'POST'
    request.POST['name'] = 'name'
    request.POST['description'] = 'description'
    request.POST['members'] = "['1', '2', '3']"
    self.assertEquals(add_team(request).status_code, 200)

class TeamUnitTestModel(TestCase):
  def setUp(self):
    user = User(username='username')
    user.save()
    team = Team(
      team_owner=user,
      name='lorem',
      description='lorem',
    )
    team.save()

  def test_create_model(self):
    self.assertEquals(Team.objects.all().count(), 1)

  def test_create_model_tostring(self):
    team = Team.objects.first()
    self.assertEquals(str(team), 'lorem is a team owned by username')

  def test_team_model_name(self):
    team = Team.objects.first()
    self.assertEquals(team.name, 'lorem')

  def test_team_model_description(self):
    team = Team.objects.first()
    self.assertEquals(team.description, 'lorem')

  def test_team_model_owner(self):
    user = User.objects.get(username='username')
    team = Team.objects.first()
    self.assertEquals(team.team_owner.pk, user.pk)
