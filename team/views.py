from django.shortcuts import render, redirect
from django.http import HttpResponseBadRequest, HttpResponseForbidden, HttpResponseNotAllowed
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required

# Models
from django.contrib.auth.models import User
from .models import Team

@login_required
def dashboard(request):
  return render(request, 'team/myteams.html')

@login_required
def create_team(request):
  context={
    'users': User.objects.exclude(pk=request.user.pk)
  }
  return render(request, 'team/create-team.html', context)

@login_required
def add_team(request):
  if request.method == 'POST':
    try:
      name = request.POST['name']
      description = request.POST['description']
      members = request.POST.getlist('members[]')
    except KeyError:
      return HttpResponseBadRequest

    # Create instance
    new_team = Team(team_owner=request.user, name=name, description=description)
    new_team.save()
    for id in members:
      new_team.members.add(int(id))

    return JsonResponse({
      'success': True
    })
  else:
    return HttpResponseNotAllowed(permitted_methods=['POST'])


@login_required
def delete_team(request, pk):
  try:
    team = Team.objects.get(pk=pk)
  except Team.DoesNotExist:
    return HttpResponseBadRequest

  if (team.team_owner == request.user):
    team.delete()
    return redirect('teams')
  else:
    return HttpResponseForbidden