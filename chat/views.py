from django.contrib.auth import login
from team.models import Team
from django.contrib.auth.models import User
from django.http.response import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import redirect, render
from .models import Message
from django.utils import timezone
from django.contrib.auth.decorators import login_required

# Create your views here.
# @login_required(login_url="/login/")
def chat(request, id):
    team_instance = Team.objects.get(id=id)
    members = team_instance.members.all()
    response = {
        "team": team_instance,
        "member_list": members
    }
    return render(request, 'chat/chats.html', response)

@login_required(login_url="/login/")
def get(request, id):
    team_instance = Team.objects.get(id=id)
    messagesList = Message.objects.filter(team=team_instance)
    messagesJSON = []
    for msg in messagesList:
        author = msg.author.username
        messagesJSON.append({"id":msg.id, "author":author, "message":msg.message, "time":msg.time})
    return JsonResponse({"data":messagesJSON})

@login_required(login_url="/login/")
def add_message(request, id):
    if request.method == 'POST':
        team_instance = Team.objects.get(id=id)
        newMessage = Message.objects.create(author=request.user, team=team_instance ,message=request.POST['messages'], time=timezone.now())
    return HttpResponseRedirect(f"/team/{id}/chat")