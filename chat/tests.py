import time
from django.contrib.auth import authenticate
from django.http import response
from django.test import TestCase, Client, override_settings
from .models import Message
from django.contrib.auth.models import User
from django.utils import timezone
# import sys
# sys.path.append('..')
from team.models import Team

# Create your tests here.
@override_settings(STATICFILES_STORAGE='django.contrib.staticfiles.storage.StaticFilesStorage')
class Test(TestCase):
    def test_model_message(self):
        user = User.objects.create(username="test", password="123456")
        team_instance = Team(team_owner=user, name='name', description='description')     
        team_instance.save()
        Message.objects.create(author=user, team=team_instance, message="test message", time=timezone.now())
        hitung = Message.objects.all().count()
        self.assertEquals(hitung, 1)

    def test_url_chat(self):
        user = User.objects.create(username="test", password="123456")        
        team_instance = Team(team_owner=user, name='name', description='description')     
        team_instance.save()
        response = Client().get(f'/team/{team_instance.id}/chat/')
        self.assertEquals(response.status_code, 200)

    # def test_html_setelah_add_message(self):
    #     user = User.objects.create(username="test", password="123456")        
    #     team_instance = Team(team_owner=user, name='name', description='description')     
    #     team_instance.save()
    #     newMessage = Message.objects.create(author=user, team=team_instance, message="test message", time=timezone.now())
    #     response = Client().get(f'/team/{team_instance.id}/chat/get')
    #     html_kembalian = response.content.decode('utf8')
    #     print(response.json())
    #     self.assertIn("test message", html_kembalian)