from django.urls import path
from . import views

app_name = 'acc'
urlpatterns = [
    path('', views.loginPage, name = 'loginPage'),
    path('logout/', views.logoutUser, name='logout'),
    path('register/', views.register, name = 'register'),
]