from django.test import TestCase, Client
from django.contrib.auth.models import User
from django.urls import resolve, reverse
from .views import register, loginPage, logoutUser
from .forms import CreatUserForm

class testAccount(TestCase):
    def test_url_login_ada(self):
        response = Client().get('/login/')
        self.assertEquals(response.status_code, 200)
    
    def test_url_login_ada_templatenya(self):
        response = Client().get('/login/')
        self.assertTemplateUsed(response, 'login/login.html')
    
    def test_login_views(self):
        found = resolve('/login/')
        self.assertEqual(found.func, loginPage)

    def test_url_register_ada(self):
        response = Client().get('/login/register/')
        self.assertEquals(response.status_code, 200)
    
    def test_url_register_ada_templatenya(self):
        response = Client().get('/login/register/')
        self.assertTemplateUsed(response, 'register/register.html')
    
    def test_regiter_views_ada(self):
        found = resolve('/login/register/')
        self.assertEqual(found.func, register)

    def test_model(self):
        User.objects.create(username='aa', email='bb')
        total = User.objects.all().count()
        self.assertEqual(total,1)

    def test_register_berhasil(self):
        user = User(username='username')
        user.save()
        jumlah = User.objects.filter(username='username').count()
        self.assertEqual(jumlah, 1)
    
    def test_invalid_register_form(self):
        w = User.objects.create(username='username', email='usercom')
        data = {'username': w.username, 'email': w.email}
        form = CreatUserForm(data=data)
        self.assertFalse(form.is_valid())
