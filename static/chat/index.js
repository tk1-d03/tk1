const getMessage = async (anchor, url) => {
    let prevChats = document.getElementById("messages").innerHTML
    const response = await fetch(url.concat("get"))
    if (response.ok) {
        const jsonResponse = await response.json();
        let stringIsi = ""
        jsonResponse.data.map(msg => (
            stringIsi = stringIsi.concat(`<div class="message">
            <span class="author">${msg.author}</span>
            <span class="time">${msg.time.slice(0,10)}  ${msg.time.slice(11,msg.time.length-8)}</span>
            <p>${msg.message}</p>
          </div>`)
        ))
        document.getElementById("messages").innerHTML = stringIsi
        if (prevChats != stringIsi){
            anchor.scrollIntoView()
        }
    }
}

// const id = window.location.pathname.slice(window.location.pathname.length-7, window.location.pathname.length-6)
const anchor = document.getElementById("anchor")
const url = window.location.href

getMessage(anchor, url)

setInterval(() => {
    getMessage(anchor, url)
}, 1000);
if(window.location.pathname != "/chat/"){
    clearInterval()
}
