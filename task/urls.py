from django.urls import path
from . import views

app_name = 'task'
urlpatterns = [
    path('employee/', views.show_task, name = 'employee'),
    path('add/', views.add_task, name='add_task'),
    path('employee/submit/<int:pk>', views.submit_task, name='submit_task')
]