from django.shortcuts import render,redirect
from django.http import HttpResponseBadRequest
from .models import Task
from .forms import TaskForm
from . import forms
from team.models import Team
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required(login_url="/login/")
def add_task(request, id) :
    task_form = forms.TaskForm(id)
    team_instance = Team.objects.get(id=id)
    members = team_instance.members.all()
    try :
        team = Team.objects.get(id=id)
    except Team.DoesNotExist :
        return HttpResponseBadRequest()

    if request.method == "POST":
        task_input = forms.TaskForm(id, request.POST)
        if task_input.is_valid():
            data = task_input.cleaned_data
            new_task = Task()
            new_task.team = team
            new_task.task_name = data['task_name']
            new_task.description = data['description']
            new_task.member = data['member']
            new_task.save()
            return redirect(f'/dashM/{id}/')
        else :
            current_data = Task.objects.filter(team = Team.objects.get(id=id)).filter(member=request.user).filter(status = 'on_going')
            return render(request, 'taskForm.html',{'form': task_form, 'status':'failed','data':current_data, "team": team_instance, "member_list": members, 'team_id' : id  })
    else:
        current_data = Task.objects.filter(member=request.user).filter(status = 'on_going')
        return render(request, 'taskForm.html' ,{'form': task_form,'data':current_data, "team": team_instance, "member_list": members, 'team_id' : id })

@login_required(login_url="/login/")
def show_task(request, id) :
    task = Task.objects.filter(team = Team.objects.get(id=id)).filter(member=request.user).filter(status = 'on_going')
    team_instance = Team.objects.get(id=id)
    members = team_instance.members.all()
    response = {
        'tasks': task,
        "team": team_instance,
        "member_list": members,
        "team_id" : id,
    }
    return render(request,'employee.html', response)
    
@login_required(login_url="/login/")
def submit_task(request, id, pk) :
    if request.method == 'POST' :
        task = Task.objects.filter(team = Team.objects.get(id=id)).filter(member=request.user).get(id=pk)
        task.status = 'submitted'
        task.save()
        return redirect(f'/team/{id}/task/employee/')