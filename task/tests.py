from django.test import TestCase, Client
from django.urls import reverse, resolve;
from .models import Task
from team.models import Team
from .forms import TaskForm
from .views import add_task, show_task, submit_task
from django.contrib.auth.models import User
from django.http import HttpRequest
from django.apps import apps
from .apps import TaskConfig

# Test Model
class Model_Tests(TestCase) :
    def test_model_can_add_new_task(self):
        user = User(username='raniah', password = '12345')
        user.save()
        team = Team(team_owner=user, name='team1', description='team1')
        team.save()
        task = Task.objects.create(task_name='task', description = 'task description', member = user, team = team , status='on_going')
        count_tasks = Task.objects.all().count()
        self.assertEqual(count_tasks, 1)

# Test Forms
class Forms_Test(TestCase) :
    def test_form_add_task(self) :
        user = User(username='raniah', password = '12345')
        user.save()

        team = Team(team_owner=user, name='team1', description='team1')
        team.save()
        team.members.add(user)

        task_input = TaskForm(data = {
            'task_name' : 'task1',
            'description' : 'desc',
            'member' : user,
        }, 
        id = team.id)
        self.assertTrue(task_input.is_valid())

    def test_task_form_validation_task_name(self):
        user = User(username='raniah', password = '12345')
        user.save()

        team = Team(team_owner=user, name='team1', description='team1')
        team.save()
        team.members.add(user)

        task_input = TaskForm(data = {
            'task_name': '',
            'description' : 'desc',
            'member' : user,
            }, 
            id = team.id)
        self.assertFalse(task_input.is_valid())
        self.assertEqual(
            task_input.errors['task_name'],
            ["This field is required."]
        )

    def test_task_form_validation_description(self):
        user = User(username='raniah', password = '12345')
        user.save()

        team = Team(team_owner=user, name='team1', description='team1')
        team.save()
        team.members.add(user)

        task_input = TaskForm(data = {
            'task_name': 'task1',
            'description' : '',
            'member' : user,
            }, 
            id = team.id)
        self.assertFalse(task_input.is_valid())
        self.assertEqual(
            task_input.errors['description'],
            ["This field is required."]
        )

    def test_task_form_validation_member(self):
        user = User(username='raniah', password = '12345')
        user.save()

        team = Team(team_owner=user, name='team1', description='team1')
        team.save()
        team.members.add(user)

        task_input = TaskForm(data = {
            'task_name': 'task1',
            'description' : 'desc',
            'member' : '',
            }, 
            id = team.id)
        self.assertFalse(task_input.is_valid())
        self.assertEqual(
            task_input.errors['member'],
            ["This field is required."]
        )

# Test URLs
class Urls_Tests(TestCase) :
    def setUp(self):
        self.user = User.objects.create(
            username="raniah",
            password='12345'
        )
        self.team = Team.objects.create(
            team_owner=self.user, 
            name='team1', 
            description='team1',
        )
        self.task = Task.objects.create(
            task_name = 'task1',
            description = 'description',
            member = self.user,
            team = self.team,
        )
        self.add_task = reverse("task:add_task", args=[self.team.id])
        self.employee = reverse('task:employee', args=[self.team.id])
        self.submit_task = reverse('task:submit_task', args=[self.team.id, self.task.id])

    def test_add_task_use_the_right_function(self):
        found = resolve(self.add_task)
        self.assertEqual(found.func, add_task)

    def test_employee_use_the_right_function(self):
        found = resolve(self.employee)
        self.assertEqual(found.func, show_task)
    
    def test_submit_task_use_the_right_function(self):
        found = resolve(self.submit_task)
        self.assertEqual(found.func, submit_task)

# Test Views and Templates
# class Views_and_Templates_Tests(TestCase) :
#     def setUp(self):
#         self.client = Client()
#         self.user = User.objects.create_user(username='raniah', password='12345')
#         self.user.save()
#         self.client.login(username='raniah', password='12345')
#         team = Team(
#             team_owner=self.user, 
#             name='team1', 
#             description='team1',
#         )
#         team.save()

#         user2 = User(username='wawa', password = 'wawa123')
#         user2.save()
#         team.members.add(user2)
#         self.add_task = reverse("task:add_task", args=[team.id])
#         self.employee = reverse('task:employee', args=[team.id])

#     def test_template_for_add_task(self):
#         response = self.client.get(self.add_task)
#         self.assertTemplateUsed(response, 'taskForm.html')
#         self.assertEqual(response.status_code, 200)

#     def test_template_for_employee_dashboard(self):
#         response = self.client.get(self.employee)
#         self.assertTemplateUsed(response, 'employee.html')
#         self.assertEqual(response.status_code, 200)
        
#     def test_add_task(self) :
#         response = self.client.post(self.add_task,
#         {
#             'task_name': 'task1',
#             'desctiption' : 'desc',
#             'member' : self.user
#         }, 
#         follow=True)
#         self.assertEqual(response.status_code, 200)

class TestApp(TestCase):
    def test_apps(self):
        self.assertEqual(TaskConfig.name, 'task')
        self.assertEqual(apps.get_app_config('task').name, 'task')
