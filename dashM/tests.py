from django.test import TestCase, Client, override_settings
from django.urls import resolve
from django.http import HttpRequest
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from .views import *
from task.models import Task
from team.models import Team

# Create your tests here.

@override_settings(STATICFILES_STORAGE='django.contrib.staticfiles.storage.StaticFilesStorage')
class TestDashM(TestCase):

    def test_landing_page(self):
        self.assertIsNotNone(dashboard)

    def test_url_ada(self):
        user = User.objects.create(username='jsaon', password='abcdefgh')
        team_instance = Team(team_owner=user, name='team1', description='test')
        team_instance.save()
        response = Client().get(f'/dashM/{team_instance.id}/')
        self.assertEqual(response.status_code, 200)

    def test_relasi_dashboard_funct(self):
        user = User.objects.create(username='jsaon', password='abcdefgh')
        team_instance = Team(team_owner=user, name='team1', description='test')
        team_instance.save()
        found = resolve(f'/dashM/{team_instance.id}/')
        self.assertEqual(found.func, dashboard)

    # def test_relasi_task_accept_funct(self):
    #     user = User.objects.create(username='jsaon', password='abcdefgh')
    #     team_instance = Team(team_owner=user, name='team1', description='test')
    #     team_instance.save()
    #     task = Task.objects.create(task_name='task1', description='description1', member=user, team=team_instance, status='on_going')
    #     task.save()
    #     task_test = task.objects.filter(team_test = team_instance.objects.get(id=id)).get(id=pk)
    #     found = resolve(f'/dashM/{team_instance.id}/accept/<int:pk>/')
    #     self.assertEqual(found.func, task_accept)

    # def test_relasi_task_reject_funct(self):
    #     user = User.objects.create(username='jsaon', password='abcdefgh')
    #     team_instance = Team(team_owner=user, name='team1', description='test')
    #     team_instance.save()
    #     task = Task.objects.create(task_name='task1', description='description1', member=user, team=team_instance, status='on_going')
    #     task.save()
    #     task_test = task.objects.filter(team_test = team_instance.objects.get(id=id)).get(id=pk)
    #     found = resolve(f'/dashM/{team_instance.id}/reject/<int:pk>/')
    #     self.assertEqual(found.func, task_reject)

    def test_template(self):
        user = User.objects.create(username='jsaon', password='abcdefgh')
        team_instance = Team(team_owner=user, name='team1', description='test')
        team_instance.save()
        response = Client().get(f'/dashM/{team_instance.id}/')
        self.assertTemplateUsed(response, 'dashMn.html')

    def test_dashboard_ada(self):
        user = User.objects.create(username='jsaon', password='abcdefgh')
        team_instance = Team(team_owner=user, name='team1', description='test')
        team_instance.save()
        request = HttpRequest()
        if request.method == 'GET' :
            response = dashboard(request, team_instance.id)
            html_response = response.content.decode('utf8')
            self.assertIn(team_instance, html_response)
    
    def test_bad_request(self):
        user = User.objects.create(username='jsaon', password='abcdefgh')
        team_instance = Team(team_owner=user, name='team1', description='test')
        team_instance.save()
        response = Client().post(f'/dashM/{team_instance.id}/', data={'user':user, 'team_instance':team_instance})
        self.assertEqual(response.status_code, 400)