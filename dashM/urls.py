from django.urls import path
from . import views

app_name = "dashM"

urlpatterns = [
	path('', views.dashboard, name = 'manager'),
	path('accept/<int:pk>/', views.task_accept, name='task_accept'),
	path('reject/<int:pk>/', views.task_reject, name='task_reject'),
	path('delete/<int:pk>/', views.task_delete, name='task_delete')
]
