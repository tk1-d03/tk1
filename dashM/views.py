from django.shortcuts import render, redirect
from task.models import Task
from task.forms import TaskForm
from team.models import Team
from django.contrib.auth.models import User
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponseBadRequest

# Create your views here.

@csrf_exempt

def dashboard(request, id) :
    if request.method == 'GET':
        taskOn = Task.objects.filter(team = Team.objects.get(id=id)).filter(status = 'on_going')
        taskSub = Task.objects.filter(team = Team.objects.get(id=id)).filter(status = 'submitted')
        team_instance = Team.objects.get(id=id)
        members = team_instance.members.all()
        
        response = {
            "task_On": taskOn,
            "task_Sub": taskSub,
            "team": team_instance,
            "member_list": members,
            "team_id" : id,
        }
        return render(request,'dashMn.html', response)
    else :
        return HttpResponseBadRequest('Bad request')

def task_accept(request, id, pk) :
    if request.method == 'GET' :
        task = Task.objects.filter(team = Team.objects.get(id=id)).get(id=pk)
        task.status = 'accepted'
        task.save()
        return redirect(f'/dashM/{id}')

def task_reject(request, id, pk) :
    if request.method == 'GET' :
        task = Task.objects.filter(team = Team.objects.get(id=id)).get(id=pk)
        task.status = 'on_going'
        task.save()
        return redirect(f'/dashM/{id}')

def task_delete(request, id, pk) :
    if request.method == 'GET' :
        task = Task.objects.filter(team = Team.objects.get(id=id)).get(id=pk)
        task.delete()
        return redirect(f'/dashM/{id}')